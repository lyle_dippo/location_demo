'use strict';

/**
 * @ngdoc overview
 * @name App
 * @description
 * # App
 *
 * Main module of the application.
 */
angular.module('App', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.ref',
    'firebase.auth',
    'ui.router'

  ])

  .config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise('/home');
  //
  // Now set up the states
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'views/templates/home.html'
    })
    .state('view-locations', {
      url: '/view-locations',
      templateUrl: 'views/templates/view-locations.html',
      controller: 'LocationsCtrl'
    })
    .state('add-locations', {
      url: '/add-locations',
      templateUrl: 'views/templates/add-locations.html',
      controller: 'LocationsCtrl'
    });

  });
