'use strict';

angular.module('App')
  .controller('LocationsCtrl', function ($scope, Ref, $firebaseArray, $timeout) {
    // synchronize a read-only, synchronized array of locations, limit to most recent 10

    $scope.locations = $firebaseArray(Ref.child('locations').limitToLast(10));
    $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 8 };
    // display any errors
    $scope.locations.$loaded().catch(alert);

    // provide a method for adding a location
    $scope.addLocation = function(newLocation) {
      if( newLocation ) {
        // push a location to the end of the array
        $scope.locations.$add({text: newLocation})
          // display any errors
          .catch(alert);
      }
    };

    function alert(loc) {
      $scope.err = loc;
      $timeout(function() {
        $scope.err = null;
      }, 5000);
    }
  });
