angular.module('firebase.config', [])
  .constant('FBURL', 'https://blazing-torch-5360.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['password'])

  .constant('loginRedirectPath', '/login');
