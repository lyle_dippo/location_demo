'use strict';

angular.module('App')
  .filter('mappify', function() {
    return function(value) {
      return (!value) ? '' : value.replace(/ /g, '+');
    };
  });
